﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW6
{
    public partial class MainPage : ContentPage
    {
        public string EntryText { get; set; }

        private ObservableCollection<WordItem> definitions = null; //list of possible definitions of a word

        public MainPage()
        {
            BindingContext = this;
            definitions = new ObservableCollection<WordItem>();
            InitializeComponent();
            DictCarousel.ItemsSource = definitions;

        }

        async void OnRequest(object sender, EventArgs e)
        {
            definitions.Clear();
            HttpClient client = new HttpClient();

            //retrieve json data of the searched word
            Uri uri = new Uri("https://owlbot.info/api/v2/dictionary/" + EntryText + "?format=json");

            //create http request
            HttpRequestMessage request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;

            //attempt to send request to website
            HttpResponseMessage response = null;
            try
            {
                response = await client.SendAsync(request);
            }catch(Exception ex)
            {
                //if it fails, displays an alert to user and ends the function
                await DisplayAlert("Connection error", "Sorry, something went wrong! Make sure you are connected to the Internet", "OK");
                return;
            }
            WordInfo[] wordData = null;

            //response has been given and it is a valid word
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                //try to convert the received data
                try
                {
                    wordData = WordInfo.FromJson(content);
                }
                catch (Exception ex)
                {
                    //if json is bad, displays error message and returns the function
                    await DisplayAlert("Data error", "Something went wrong while retrieving data. Please try again!", "OK");
                    return;
                }

                //gets every definition of the word and adds to the definitions list to be displayed
                for(int i = 0; i < wordData.Length; i++)
                {
                    string type = "";
                    string desc = "";
                    string example = "";
                    //ifs are needed because some fields may be null
                    if(wordData[i].Type != null)
                    {
                        type = wordData[i].Type;
                    }
                    if (wordData[i].Definition != null)
                    {
                        desc = wordData[i].Definition;
                    }
                    if (wordData[i].Example != null)
                    {
                        example = wordData[i].Example;
                    }
                    
                    definitions.Add(new WordItem {Title = EntryText, Type = type, Desc = desc, Example = example });
                }
            }
            else // response has been given but it is not a valid word
            {
                await DisplayAlert("Error", "Sorry, something has gone wrong! Please make sure your word was typed correctly.", "OK");
            }
            
        }
    }
}
