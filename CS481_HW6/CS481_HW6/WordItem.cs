﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CS481_HW6
{
    public class WordItem
    {
        //this class contains all data that needs to be displayed in each of the definitions in the carousel.
        public string Title { get; set; }
        public string Type { get; set; }
        public string Desc { get; set; }
        public string Example { get; set; }

        public WordItem()
        {

        }

        public WordItem(string title, string type, string desc, string example)
        {
            Title = title;
            Type = type;
            Desc = desc;
            Example = example;
        }
    }
}
