﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CS481_HW6
{
    //this class has been generated automatically on the website app.quicktype.io
    public partial class WordInfo
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }

    public partial class WordInfo
    {
        public static WordInfo[] FromJson(string json) => JsonConvert.DeserializeObject<WordInfo[]>(json, CS481_HW6.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this WordInfo[] self) => JsonConvert.SerializeObject(self, CS481_HW6.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
